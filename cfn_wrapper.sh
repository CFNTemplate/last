#!/bin/sh
# shellcheck disable=SC2045

stack_prefix="dofsec002-base-"
usage="$0 create|delete [opts]"

if [ $# -lt 1 ];then
  echo "$usage"
  exit 1
fi

get_stack_type() {
  template_name=$1
  echo "${template_name}"|awk -F/ '{print $NF}'|awk -F. '{print $1}'|awk -F- '{print $2}'
}

case $1 in
  create)
    shift
    for cfn_template in $(ls -1 cfn_templates/*.yaml);do 
      stack_type=$(get_stack_type "$cfn_template")
      echo "Creating $stack_type..."
      python cfn_stack.py "${@}" "${stack_prefix}${stack_type}" "${cfn_template}"
    done 
  ;;
  delete)
    shift
    for cfn_template in $(ls -1r cfn_templates/*.yaml);do 
      stack_type=$(get_stack_type "${cfn_template}")
      echo "Deleting $stack_type..."
      python cfn_stack.py "${@}" --delete "${stack_prefix}${stack_type}" "${cfn_template}"
    done
  ;;
  *)
    echo "$usage"
    exit 2
  ;;
esac