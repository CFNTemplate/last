"""
A Bitbucket Builds template for updating a CloudFormation stack
borrowed and modified from joshcb@amazon.com
"""
from __future__ import print_function
import sys
import argparse
import boto3
from botocore.exceptions import ClientError, WaiterError

_STACK_NOT_EXIST_ERROR_MSG = 'Stack with id {stack_name} does not exist'
_NO_UPDATE_ERROR_MSG = 'No updates are to be performed.'


def wait_for_stack_update_to_complete(client, stack_name, waiter_string):
    """
    Checks the update status of the CFN stack
    until it either succeeds or fails (checks every 30 seconds up to 120 failed checks)
    """
    print("Wating for stack '{}' to complete...".format(stack_name))
    try:
        waiter = client.get_waiter(waiter_string)
    except WaiterError as err:
        print("Failed to get the waiter.\n" + str(err))
        return False
    try:
        waiter.wait(StackName=stack_name)
    except WaiterError as err:
        print("The Stack Update did not successfully complete.  " \
            "Check CloudFormation events for more information. \n" + str(err))
        return False

    print("Finsihed waiting for {}".format(stack_name))
    return True


def update_stack(client, stack_name, template):
    waiter_string = 'stack_update_complete'
    print("Updating: {0}".format(stack_name))
    """
    Updates a CFN Stack with the supplied template
    """
    try:
        """
        see all parameters here:
        http://boto3.readthedocs.io/en/latest/reference/services/cloudformation.html#CloudFormation.Client.update_stack
        """
        client.update_stack(
            StackName=stack_name,
            TemplateBody=open(template, 'r').read(),
            Capabilities=['CAPABILITY_IAM'],
            # Required if parameters need to be entered
            # Parameters=[
            #     {
            #         'ParameterKey': 'string',
            #         'ParameterValue': 'string'
            #     },
            # ]
        )
    except ClientError as err:
        if _NO_UPDATE_ERROR_MSG == err.response.get('Error', {}).get('Message', ''):
            # Nothing was updated. Ignore this error and move on.
            print("{} for {}".format(_NO_UPDATE_ERROR_MSG, stack_name))
            return True
        print("Failed to update the stack.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access " + template + ".\n" + str(err))
        return False

    wait_for_stack_update_to_complete(client, stack_name, waiter_string)
    return True


def create_stack(client, stack_name, template, rollback):
    waiter_string = 'stack_create_complete'
    print("Creating: {0}".format(stack_name))
    """
    Create a CFN Stack with the supplied template
    """
    try:
        """
        see all parameters here:
        http://boto3.readthedocs.io/en/latest/reference/services/cloudformation.html#CloudFormation.Client.update_stack
        """
        client.create_stack(
            StackName=stack_name,
            TemplateBody=open(template, 'r').read(),
            DisableRollback=rollback,
            Capabilities=['CAPABILITY_IAM'],
            # Required if parameters need to be entered
            # Parameters=[
            #     {
            #         'ParameterKey': 'string',
            #         'ParameterValue': 'string'
            #     },
            # ]
        )
    except ClientError as err:
        print("Failed to update the stack.\n" + str(err))
        return False
    except IOError as err:
        print("Failed to access " + template + ".\n" + str(err))
        return False

    wait_for_stack_update_to_complete(client, stack_name, waiter_string)
    return True


def delete_stack(client, stack_name):
    waiter_string = 'stack_delete_complete'
    print("Deleting: {0}".format(stack_name))
    try:
        client.delete_stack(
            StackName=stack_name
        )
    except ClientError as err:
        print("Failed to delete the stack.\n" + str(err))
        return False

    wait_for_stack_update_to_complete(client, stack_name, waiter_string)
    return True


# https://github.com/krux/python-krux-cloud-formation/blob/master/krux_cloud_formation/cloud_formation.py
def _is_stack_exists(client, stack_name):
    try:
        # See if we can get a template for this
        client.describe_stacks(StackName=stack_name)

        # todo: need to check if the stack is in ROLLBACK_COMPLETE state and then delete it or throw an error
        # The template was successfully retrieved; the stack exists
        return True

    except ClientError as err:
        if (_STACK_NOT_EXIST_ERROR_MSG.format(stack_name=stack_name) ==
           err.response.get('Error', {}).get('Message', '')):
            # The template was not retrieved; the stack does not exists
            return False

        # Unknown error. Raise again.
        raise


def main():
    " Your favorite wrapper's favorite wrapper "
    parser = argparse.ArgumentParser(prog='cfn_stack')
    parser.add_argument("stack_name", help="The CFN stack to update")
    parser.add_argument("template", help="The CFN Template to update the stack")
    parser.add_argument("--delete", help="if true, then it will delete the stack_name", action="store_true")
    parser.add_argument("--rollback", help="if true, then it will disable rollback", action="store_true")
    args = parser.parse_args()

    stack_name = args.stack_name
    template = args.template
    stack_delete = args.delete
    rollback = args.rollback

    try:
        client = boto3.client('cloudformation')

    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False

    # test to see if the stace_name exists
    get_stack_state = _is_stack_exists(client, stack_name)

    if stack_delete:
        if get_stack_state:
            delete_stack(client, stack_name)
    else:
        if get_stack_state:
            if not update_stack(client, stack_name, template):
                sys.exit(1)
        else:
            if not create_stack(client, stack_name, template, rollback):
                sys.exit(1)


if __name__ == "__main__":
    main()
